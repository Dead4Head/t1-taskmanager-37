package ru.t1.amsmirnov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.amsmirnov.taskmanager.api.repository.ITaskRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.enumerated.TaskSort;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
import ru.t1.amsmirnov.taskmanager.model.Project;
import ru.t1.amsmirnov.taskmanager.model.Task;
import ru.t1.amsmirnov.taskmanager.service.ConnectionService;
import ru.t1.amsmirnov.taskmanager.service.PropertyService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@Category(DBCategory.class)
public class TaskRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String NONE_ID = "---NONE---";

    @NotNull
    private static final String USER_ALFA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_BETA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(new PropertyService());

    @NotNull
    private static final Project projectUserAlfa = new Project();

    @NotNull
    private Connection connection;

    @NotNull
    private List<Task> tasks;

    @NotNull
    private List<Task> alfaTasks;

    @NotNull
    private List<Task> betaTasks;

    @NotNull
    private ITaskRepository taskRepository;

    @BeforeClass
    public static void initData() {
        projectUserAlfa.setName("Test task name");
        projectUserAlfa.setDescription("Test task description");
        projectUserAlfa.setUserId(USER_ALFA_ID);
    }

    @Before
    public void initRepository() throws Exception {
        tasks = new ArrayList<>();
        connection = CONNECTION_SERVICE.getConnection();
        taskRepository = new TaskRepository(connection);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            Thread.sleep(2);
            @NotNull final Task task = new Task();
            task.setName("Task : " + (NUMBER_OF_ENTRIES - i));
            task.setDescription("Description: " + i);
            if (i <= 5)
                task.setUserId(USER_ALFA_ID);
            else
                task.setUserId(USER_BETA_ID);
            if (i % 3 == 0)
                task.setProjectId(projectUserAlfa.getId());
            try {

                tasks.add(task);
                taskRepository.add(task);
            } catch (final SQLException e) {
                connection.rollback();
                throw e;
            }
        }
        alfaTasks = tasks
                .stream()
                .filter(t -> t.getUserId().equals(USER_ALFA_ID))
                .collect(Collectors.toList());
        betaTasks = tasks
                .stream()
                .filter(t -> t.getUserId().equals(USER_BETA_ID))
                .collect(Collectors.toList());
    }

    @After
    public void clearRepository() throws SQLException {
        try {
            taskRepository.removeAll();
            tasks.clear();
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }


    @Test
    public void testAdd() throws SQLException {
        @NotNull final Task task = new Task();
        task.setName("Test task name");
        task.setDescription("Test task description");
        task.setUserId(USER_ALFA_ID);
        try {
            taskRepository.add(USER_ALFA_ID, task);
            tasks.add(task);
            List<Task> actualTasks = taskRepository.findAll();
            assertEquals(tasks, actualTasks);
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testRemoveAll() throws SQLException {
        try {
            assertNotEquals(0, taskRepository.getSize());
            taskRepository.removeAll();
            assertEquals(0, taskRepository.getSize());
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testRemoveAllForUser() throws SQLException {
        try {
            assertNotEquals(0, taskRepository.getSize(USER_ALFA_ID));
            taskRepository.removeAll(USER_ALFA_ID);
            assertEquals(0, taskRepository.getSize(USER_ALFA_ID));
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testRemoveAllList() throws SQLException {
        try {
            assertNotEquals(0, taskRepository.getSize(USER_ALFA_ID));
            final List<Task> alfaTasks = taskRepository.findAll(USER_ALFA_ID);
            taskRepository.removeAll(alfaTasks);
            assertEquals(0, taskRepository.getSize(USER_ALFA_ID));
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testExistById() throws SQLException {
        try {
            for (final Task task : tasks) {
                assertTrue(taskRepository.existById(task.getUserId(), task.getId()));
            }
            assertFalse(taskRepository.existById(USER_ALFA_ID, UUID.randomUUID().toString()));
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testFindAll() throws SQLException {
        try {
            assertNotEquals(taskRepository.findAll(USER_ALFA_ID), taskRepository.findAll(USER_BETA_ID));

            assertEquals(alfaTasks, taskRepository.findAll(USER_ALFA_ID));
            assertEquals(betaTasks, taskRepository.findAll(USER_BETA_ID));
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Ignore
    public void testFindAllComparator() throws SQLException {
        try {
            alfaTasks.sort(TaskSort.BY_NAME.getComparator());
            assertEquals(alfaTasks, taskRepository.findAll(USER_ALFA_ID, TaskSort.BY_NAME.getComparator()));
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testFindOneById() throws SQLException {
        try {
            for (final Task task : tasks) {
                assertEquals(task, taskRepository.findOneById(task.getUserId(), task.getId()));
            }
            assertNull(taskRepository.findOneById(NONE_ID));
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testFindOneByIndex() throws SQLException {
        try {
            for (int i = 0; i < alfaTasks.size(); i++)
                assertEquals(alfaTasks.get(i), taskRepository.findOneByIndex(USER_ALFA_ID, i));
            for (int i = 0; i < betaTasks.size(); i++)
                assertEquals(betaTasks.get(i), taskRepository.findOneByIndex(USER_BETA_ID, i));
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testGetSize() throws SQLException {
        try {
            assertEquals(tasks.size(), taskRepository.getSize());
            final long alfaTasksCount = tasks
                    .stream()
                    .filter(t -> t.getUserId().equals(USER_ALFA_ID))
                    .count();
            final long betaTasksSize = tasks
                    .stream()
                    .filter(t -> t.getUserId().equals(USER_BETA_ID))
                    .count();
            assertEquals(alfaTasksCount, taskRepository.getSize(USER_ALFA_ID));
            assertEquals(betaTasksSize, taskRepository.getSize(USER_BETA_ID));
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testRemove() throws SQLException {
        try {
            for (final Task task : tasks) {
                assertTrue(taskRepository.existById(task.getUserId(), task.getId()));
                taskRepository.removeOne(task.getUserId(), task);
                assertFalse(taskRepository.existById(task.getUserId(), task.getId()));
            }
            assertNull(taskRepository.removeOne(USER_ALFA_ID, new Task()));
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testRemoveById() throws SQLException {
        try {
            for (final Task task : tasks) {
                assertTrue(taskRepository.existById(task.getId()));
                taskRepository.removeOneById(task.getUserId(), task.getId());
                assertFalse(taskRepository.existById(task.getId()));
            }
            assertNull(taskRepository.removeOneById(USER_ALFA_ID, NONE_ID));
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testRemoveByIndex() throws SQLException {
        try {
            for (final Task task : tasks) {
                if (task.getUserId().equals(USER_ALFA_ID)) {
                    assertTrue(taskRepository.existById(task.getUserId(), task.getId()));
                    taskRepository.removeOneByIndex(task.getUserId(), 0);
                    assertFalse(taskRepository.existById(task.getUserId(), task.getId()));
                }
                if (task.getUserId().equals(USER_BETA_ID)) {
                    assertTrue(taskRepository.existById(task.getUserId(), task.getId()));
                    taskRepository.removeOneByIndex(task.getUserId(), 0);
                    assertFalse(taskRepository.existById(task.getUserId(), task.getId()));
                }
            }
            assertNull(taskRepository.removeOneByIndex(USER_ALFA_ID, 0));
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testFindAllByProjectId() throws SQLException {
        final List<Task> alfaTasksWithProject = alfaTasks
                .stream()
                .filter(t -> projectUserAlfa.getId().equals(t.getProjectId()))
                .collect(Collectors.toList());
        final List<Task> betaTasksWithProject = tasks
                .stream()
                .filter(t -> t.getUserId().equals(USER_BETA_ID) && projectUserAlfa.getId().equals(t.getProjectId()))
                .collect(Collectors.toList());
        try {
            final List<Task> alfaRepoTasks = taskRepository.findAllByProjectId(USER_ALFA_ID, projectUserAlfa.getId());
            final List<Task> betaRepoTasks = taskRepository.findAllByProjectId(USER_BETA_ID, projectUserAlfa.getId());
            assertNotEquals(alfaRepoTasks, betaRepoTasks);
            assertEquals(alfaTasksWithProject, alfaRepoTasks);
            assertEquals(betaTasksWithProject, betaRepoTasks);

            assertEquals(new ArrayList<>(), taskRepository.findAllByProjectId(USER_ALFA_ID, NONE_ID));
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

}
