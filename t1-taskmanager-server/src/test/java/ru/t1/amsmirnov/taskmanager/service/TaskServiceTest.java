package ru.t1.amsmirnov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.api.service.ITaskService;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.enumerated.TaskSort;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.field.*;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
import ru.t1.amsmirnov.taskmanager.model.Project;
import ru.t1.amsmirnov.taskmanager.model.Task;
import ru.t1.amsmirnov.taskmanager.repository.ProjectRepository;
import ru.t1.amsmirnov.taskmanager.repository.ProjectRepositoryTest;
import ru.t1.amsmirnov.taskmanager.repository.TaskRepository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.UUID.randomUUID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@Category(DBCategory.class)
public class TaskServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String NONE_STR = "---NONE---";

    @Nullable
    private static final String NULL_STR = null;

    @NotNull
    private static final String USER_ALFA_ID = randomUUID().toString();

    @NotNull
    private static final String USER_BETA_ID = randomUUID().toString();

    @NotNull
    private static final Project projectAlfa = new Project();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(new PropertyService());

    @NotNull
    private static final ITaskService TASK_SERVICE = new TaskService(CONNECTION_SERVICE);

    @NotNull
    private List<Task> tasks;

    @NotNull
    private List<Task> alfaTasks;

    @NotNull
    private List<Task> betaTasks;


    @BeforeClass
    public static void initData() {
        projectAlfa.setName("Alfa project");
        projectAlfa.setDescription("Alfa description");
        projectAlfa.setUserId(USER_ALFA_ID);
    }

    @Before
    public void initRepository() throws Exception {
        tasks = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            Thread.sleep(2);
            @NotNull final Task task = new Task();
            task.setName("Task name: " + (NUMBER_OF_ENTRIES - i));
            task.setDescription("Task description: " + i);
            if (i <= 5) {
                task.setUserId(USER_ALFA_ID);
                task.setProjectId(projectAlfa.getId());
            } else
                task.setUserId(USER_BETA_ID);
            TASK_SERVICE.add(task);
            tasks.add(task);
        }
        tasks = tasks.stream().sorted(TaskSort.BY_CREATED.getComparator()).collect(Collectors.toList());
        alfaTasks = tasks
                .stream()
                .filter(t -> t.getUserId().equals(USER_ALFA_ID))
                .sorted(TaskSort.BY_CREATED.getComparator())
                .collect(Collectors.toList());
        betaTasks = tasks
                .stream()
                .filter(t -> t.getUserId().equals(USER_BETA_ID))
                .sorted(TaskSort.BY_CREATED.getComparator())
                .collect(Collectors.toList());
    }

    @After
    public void clearRepository() throws SQLException {
        TASK_SERVICE.removeAll();
    }

    @Test
    public void testChangeTaskStatusById() throws AbstractException, SQLException {
        int i = 0;
        for (final Task task : tasks) {
            if (i % 2 == 0) {
                TASK_SERVICE.changeStatusById(task.getUserId(), task.getId(), Status.IN_PROGRESS);
                assertEquals(Status.IN_PROGRESS, TASK_SERVICE.findOneById(task.getId()).getStatus());
                TASK_SERVICE.changeStatusById(task.getUserId(), task.getId(), null);
                assertEquals(Status.IN_PROGRESS, TASK_SERVICE.findOneById(task.getId()).getStatus());
            }
            if (i % 3 == 0) {
                TASK_SERVICE.changeStatusById(task.getUserId(), task.getId(), Status.COMPLETED);
                assertEquals(Status.COMPLETED, TASK_SERVICE.findOneById(task.getId()).getStatus());
            }
            i++;
        }
    }

    @Test(expected = IdEmptyException.class)
    public void testChangeTaskStatusById_IdEmptyException_1() throws AbstractException, SQLException {
        TASK_SERVICE.changeStatusById(NONE_STR, "", Status.IN_PROGRESS);
    }

    @Test(expected = IdEmptyException.class)
    public void testChangeTaskStatusById_IdEmptyException_2() throws AbstractException, SQLException {
        TASK_SERVICE.changeStatusById(NONE_STR, NULL_STR, Status.IN_PROGRESS);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeTaskStatusById_UserEmptyException_1() throws AbstractException, SQLException {
        TASK_SERVICE.changeStatusById("", NONE_STR, Status.IN_PROGRESS);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeTaskStatusById_UserEmptyException_2() throws AbstractException, SQLException {
        TASK_SERVICE.changeStatusById(NULL_STR, NONE_STR, Status.IN_PROGRESS);
    }

    @Test
    public void testChangeTaskStatusByIndex() throws AbstractException, SQLException {
        for (int i = 0; i < alfaTasks.size(); i++) {
            assertNotEquals(Status.IN_PROGRESS, alfaTasks.get(i).getStatus());
            TASK_SERVICE.changeStatusByIndex(USER_ALFA_ID, i, Status.IN_PROGRESS);
            assertEquals(Status.IN_PROGRESS, TASK_SERVICE.findOneById(alfaTasks.get(i).getId()).getStatus());
            TASK_SERVICE.changeStatusByIndex(USER_ALFA_ID, i, null);
            assertEquals(Status.IN_PROGRESS, TASK_SERVICE.findOneById(alfaTasks.get(i).getId()).getStatus());
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeTaskStatusByIndex_IncorrectIndexException_1() throws AbstractException, SQLException {
        TASK_SERVICE.changeStatusByIndex(USER_ALFA_ID, -1, Status.IN_PROGRESS);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeTaskStatusByIndex_IncorrectIndexException_2() throws AbstractException, SQLException {
        TASK_SERVICE.changeStatusByIndex(USER_ALFA_ID, tasks.size(), Status.IN_PROGRESS);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeTaskStatusByIndex_IncorrectIndexException_3() throws AbstractException, SQLException {
        TASK_SERVICE.changeStatusByIndex(USER_ALFA_ID, null, Status.IN_PROGRESS);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeTaskStatusByIndex_UserEmptyException_1() throws AbstractException, SQLException {
        TASK_SERVICE.changeStatusByIndex("", 0, Status.IN_PROGRESS);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeTaskStatusByIndex_UserEmptyException_2() throws AbstractException, SQLException {
        TASK_SERVICE.changeStatusByIndex(NULL_STR, 0, Status.IN_PROGRESS);
    }

    @Test
    public void testCreate() throws AbstractException, SQLException {
        final String name = "TEST project";
        final String description = "Description";
        final Task task = TASK_SERVICE.create(USER_ALFA_ID, name, description);
        final Task actualTask = TASK_SERVICE.findOneById(task.getId());
        assertEquals(name, actualTask.getName());
        assertEquals(description, actualTask.getDescription());
        assertEquals(USER_ALFA_ID, actualTask.getUserId());
        assertEquals(TASK_SERVICE.getSize(), tasks.size() + 1);
    }

    @Test(expected = NameEmptyException.class)
    public void testCreate_NameException_1() throws AbstractException, SQLException {
        final String description = "Description";
        TASK_SERVICE.create(USER_ALFA_ID, "", description);
    }

    @Test(expected = NameEmptyException.class)
    public void testCreate_NameException_2() throws AbstractException, SQLException {
        final String description = "Description";
        TASK_SERVICE.create(USER_ALFA_ID, NULL_STR, description);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreate_UserEmptyException_1() throws AbstractException, SQLException {
        final String description = "Description";
        TASK_SERVICE.create("", NONE_STR, description);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreate_UserEmptyException_2() throws AbstractException, SQLException {
        final String description = "Description";
        TASK_SERVICE.create(NULL_STR, NONE_STR, description);
    }

    @Test
    public void testFindAllByProjectId() throws AbstractException, SQLException {
        final List<Task> alfaTasksByProject = alfaTasks
                .stream()
                .filter(t -> projectAlfa.getId().equals(t.getProjectId()))
                .collect(Collectors.toList());
        final List<Task> betaTasksByProject = betaTasks
                .stream()
                .filter(t -> projectAlfa.getId().equals(t.getProjectId()))
                .collect(Collectors.toList());
        final List<Task> alfaServTasks = TASK_SERVICE.findAllByProjectId(USER_ALFA_ID, projectAlfa.getId());
        final List<Task> betaServTasks = TASK_SERVICE.findAllByProjectId(USER_BETA_ID, projectAlfa.getId());
        assertNotEquals(alfaServTasks, betaServTasks);
        assertEquals(alfaTasksByProject, alfaServTasks);
        assertEquals(betaTasksByProject, betaServTasks);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllByProjectId_UserEmptyException_1() throws AbstractException, SQLException {
        TASK_SERVICE.findAllByProjectId("", projectAlfa.getId());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllByProjectId_UserEmptyException_2() throws AbstractException, SQLException {
        TASK_SERVICE.findAllByProjectId(NULL_STR, projectAlfa.getId());
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testFindAllByProjectId_ProjectEmptyException_1() throws AbstractException, SQLException {
        TASK_SERVICE.findAllByProjectId(NONE_STR, "");
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testFindAllByProjectId_ProjectEmptyException_2() throws AbstractException, SQLException {
        TASK_SERVICE.findAllByProjectId(NONE_STR, NULL_STR);
    }

    @Test
    public void testUpdateById() throws AbstractException, SQLException {
        for (final Task task : tasks) {
            final String name = task.getName() + "TEST";
            final String description = task.getDescription() + "TEST";
            TASK_SERVICE.updateById(task.getUserId(), task.getId(), name, description);
            assertEquals(name, TASK_SERVICE.findOneById(task.getId()).getName());
            assertEquals(description, TASK_SERVICE.findOneById(task.getId()).getDescription());
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateById_UserEmptyException_1() throws AbstractException, SQLException {
        TASK_SERVICE.updateById("", NONE_STR, NONE_STR, NONE_STR);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateById_UserEmptyException_2() throws AbstractException, SQLException {
        TASK_SERVICE.updateById(NULL_STR, NONE_STR, NONE_STR, NONE_STR);
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateById_IdException_1() throws AbstractException, SQLException {
        TASK_SERVICE.updateById(NONE_STR, "", NONE_STR, NONE_STR);
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateById_IdException_2() throws AbstractException, SQLException {
        TASK_SERVICE.updateById(NONE_STR, NULL_STR, NONE_STR, NONE_STR);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateById_NameEmptyException_1() throws AbstractException, SQLException {
        TASK_SERVICE.updateById(NONE_STR, NONE_STR, "", NONE_STR);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateById_NameEmptyException_2() throws AbstractException, SQLException {
        TASK_SERVICE.updateById(NONE_STR, NONE_STR, NULL_STR, NONE_STR);
    }

    @Test
    public void testUpdateByIndex() throws AbstractException, SQLException {
        for (int i = 0; i < alfaTasks.size(); i++) {
            final String name = alfaTasks.get(i).getName() + "TEST";
            final String description = alfaTasks.get(i).getDescription() + "TEST";
            TASK_SERVICE.updateByIndex(alfaTasks.get(i).getUserId(), i, name, description);
            assertEquals(name, TASK_SERVICE.findOneById(alfaTasks.get(i).getId()).getName());
            assertEquals(description, TASK_SERVICE.findOneById(alfaTasks.get(i).getId()).getDescription());
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIndex_UserEmptyException_1() throws AbstractException, SQLException {
        TASK_SERVICE.updateByIndex("", 0, NONE_STR, NONE_STR);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIndex_UserEmptyException_2() throws AbstractException, SQLException {
        TASK_SERVICE.updateByIndex(NULL_STR, 0, NONE_STR, NONE_STR);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIndex_IndexException_1() throws AbstractException, SQLException {
        TASK_SERVICE.updateByIndex(NONE_STR, null, NONE_STR, NONE_STR);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIndex_IndexException_2() throws AbstractException, SQLException {
        TASK_SERVICE.updateByIndex(NONE_STR, -1, NONE_STR, NONE_STR);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIndex_IndexException_3() throws AbstractException, SQLException {
        TASK_SERVICE.updateByIndex(NONE_STR, alfaTasks.size(), NONE_STR, NONE_STR);
    }


    @Test(expected = NameEmptyException.class)
    public void testUpdateByIndex_NameException_1() throws AbstractException, SQLException {
        TASK_SERVICE.updateByIndex(USER_ALFA_ID, 0, "", NONE_STR);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIndex_NameException_2() throws AbstractException, SQLException {
        TASK_SERVICE.updateByIndex(USER_ALFA_ID, 0, NULL_STR, NONE_STR);
    }

//    @Test
//    public void clear() throws SQLException{
//        TaskService t = new TaskService(CONNECTION_SERVICE);
//        t.removeAll();
//        ProjectService p = new ProjectService(CONNECTION_SERVICE);
//        p.removeAll();
//        SessionService s = new SessionService(CONNECTION_SERVICE);
//        s.removeAll();
//        UserService u = new UserService(CONNECTION_SERVICE, p,t, new PropertyService());
//        u.removeAll();
//    }

}
