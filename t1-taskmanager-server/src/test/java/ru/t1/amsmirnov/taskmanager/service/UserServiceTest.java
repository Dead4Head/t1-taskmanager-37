package ru.t1.amsmirnov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.amsmirnov.taskmanager.api.service.*;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.UserNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.EmailEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.IdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.LoginEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.PasswordEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.user.EmailExistException;
import ru.t1.amsmirnov.taskmanager.exception.user.LoginExistException;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
import ru.t1.amsmirnov.taskmanager.model.Project;
import ru.t1.amsmirnov.taskmanager.model.Task;
import ru.t1.amsmirnov.taskmanager.model.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Category(DBCategory.class)
public class UserServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String NONE_STR = "---NONE---";

    @Nullable
    private static final String NULL_STR = null;

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();
    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private final IProjectService projectService = new ProjectService(CONNECTION_SERVICE);

    @NotNull
    private final ITaskService taskService = new TaskService(CONNECTION_SERVICE);

    @NotNull
    private final IUserService userService = new UserService(CONNECTION_SERVICE, projectService, taskService, PROPERTY_SERVICE);

    @NotNull
    private final List<User> users = new ArrayList<>();

    @Before
    public void initRepository() throws Exception {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            Thread.sleep(1);
            @NotNull final User user = new User();
            user.setFirstName("UserFirstName " + i);
            user.setLastName("UserLastName " + i);
            user.setMiddleName("UserMidName " + i);
            user.setEmail("user" + i + "@dot.ru");
            user.setLogin("USER" + (NUMBER_OF_ENTRIES - i));
            user.setPasswordHash(user.getId());
            user.setRole(Role.USUAL);
            userService.add(user);
            users.add(user);

            for (int j = 0; j < NUMBER_OF_ENTRIES / 4; j++) {
                Thread.sleep(1);
                @NotNull final Project project = new Project();
                project.setName("Project name: " + i + " " + j);
                project.setDescription("Project description: " + i + " " + j);
                project.setUserId(user.getId());
                projectService.add(project);

                for (int a = 0; a < NUMBER_OF_ENTRIES / 4; a++) {
                    Thread.sleep(1);
                    @NotNull final Task task = new Task();
                    task.setName("Task name: " + a);
                    task.setDescription("Task description: " + a);
                    task.setUserId(user.getId());
                    task.setProjectId(project.getId());
                    taskService.add(task);
                }
            }
        }
    }

    @After
    public void clearRepository() throws SQLException {
        projectService.removeAll();
        taskService.removeAll();
        userService.removeAll();
    }

    @AfterClass
    public static void waitTest() throws Exception {
    }

    @Test
    public void testCreate() throws AbstractException, SQLException {
        List<User> usersRepo = userService.findAll();
        assertEquals(users, usersRepo);

        User user = userService.create("TEST_USER1", "TEST_PASSWORD", "testuser1@t.com");
        users.add(user);
        users.add(userService.create("TEST_USER2", "TEST_PASSWORD", "user@mail.com"));
        users.add(userService.create("TEST_ADMIN", "TESTADMIN", "TEST_ADMIN@e.com", Role.ADMIN));
        usersRepo = userService.findAll();
        assertEquals(users.size(), usersRepo.size());
    }

    @Test(expected = EmailExistException.class)
    public void testCreate_EmailExistException_1() throws AbstractException, SQLException {
        userService.create("LOGIN1", "TEST_PASSWORD", "test@test.test");
        userService.create("LOGIN2", "TEST_PASSWORD", "test@test.test");
    }

    @Test(expected = LoginEmptyException.class)
    public void testCreate_EmptyLoginException_1() throws AbstractException, SQLException {
        userService.create("", "TEST_PASSWORD", "newtest@t.com");
    }

    @Test(expected = LoginEmptyException.class)
    public void testCreate_EmptyLoginException_2() throws AbstractException, SQLException {
        userService.create(NULL_STR, "TEST_PASSWORD", "newtest@t.com");
    }

    @Test(expected = LoginExistException.class)
    public void testCreate_LoginExistException() throws AbstractException, SQLException {
        userService.create(NONE_STR, "TEST_PASSWORD", "newtest@t.com");
        userService.create(NONE_STR, "TEST_PASSWORD", "newtest@t.com");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testCreate_EmptyPasswordException_1() throws AbstractException, SQLException {
        userService.create("LOGIN", "", "newtest@t.com");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testCreate_EmptyPasswordException_2() throws AbstractException, SQLException {
        userService.create("LOGIN", NULL_STR, "newtest@t.com");
    }

    @Test
    public void testFindByEmail() throws AbstractException, SQLException {
        for (final User user : users) {
            final User actualUser = userService.findOneByEmail(user.getEmail());
            assertEquals(user, actualUser);
        }
    }

    @Test(expected = EmailEmptyException.class)
    public void testFindByEmail_EmailEmptyException_1() throws AbstractException, SQLException {
        userService.findOneByEmail("");
    }

    @Test(expected = EmailEmptyException.class)
    public void testFindByEmail_EmailEmptyException_2() throws AbstractException, SQLException {
        userService.findOneByEmail(NULL_STR);
    }

    @Test(expected = UserNotFoundException.class)
    public void testFindByEmail_UserNotFoundException() throws AbstractException, SQLException {
        userService.findOneByEmail(NONE_STR);
    }

    @Test
    public void testFindByLogin() throws AbstractException, SQLException {
        for (final User user : users) {
            final User actualUser = userService.findOneByLogin(user.getLogin());
            assertEquals(user, actualUser);
        }
    }

    @Test(expected = LoginEmptyException.class)
    public void testFindByLogin_LoginEmptyException_1() throws AbstractException, SQLException {
        userService.findOneByLogin("");
    }

    @Test(expected = LoginEmptyException.class)
    public void testFindByLogin_LoginEmptyException_2() throws AbstractException, SQLException {
        userService.findOneByLogin(NULL_STR);
    }

    @Test(expected = UserNotFoundException.class)
    public void testFindByLogin_UserNotFoundException() throws AbstractException, SQLException {
        userService.findOneByLogin(NONE_STR);
    }

    @Test
    public void testIsEmailExist() throws AbstractException, SQLException {
        for (final User user : users) {
            assertTrue(userService.isEmailExist(user.getEmail()));
            assertFalse(userService.isEmailExist(user.getEmail() + user.getEmail()));
        }
    }

    @Test(expected = EmailEmptyException.class)
    public void testIsEmailExists_LoginEmptyException_1() throws AbstractException, SQLException {
        userService.isEmailExist("");
    }

    @Test(expected = EmailEmptyException.class)
    public void testIsEmailExists_LoginEmptyException_2() throws AbstractException, SQLException {
        userService.isEmailExist(NULL_STR);
    }


    @Test
    public void testIsLoginExist() throws AbstractException, SQLException {
        for (final User user : users) {
            assertTrue(userService.isLoginExist(user.getLogin()));
            assertFalse(userService.isLoginExist(user.getLogin() + user.getLogin()));
        }
    }

    @Test(expected = LoginEmptyException.class)
    public void testIsLoginExists_LoginEmptyException_1() throws AbstractException, SQLException {
        userService.isLoginExist("");
    }

    @Test(expected = LoginEmptyException.class)
    public void testIsLoginExists_LoginEmptyException_2() throws AbstractException, SQLException {
        userService.isLoginExist(NULL_STR);
    }

    @Test
    public void testLockUserByLogin() throws AbstractException, SQLException {
        for (final User user : users) {
            user.setLocked(false);
            assertFalse(user.isLocked());
            userService.lockUserByLogin(user.getLogin());
            assertTrue(userService.findOneById(user.getId()).isLocked());
        }
    }

    @Test(expected = LoginEmptyException.class)
    public void testLockUserByLogin_LoginEmptyException_1() throws AbstractException, SQLException {
        userService.lockUserByLogin("");
    }

    @Test(expected = LoginEmptyException.class)
    public void testLockUserByLogin_LoginEmptyException_2() throws AbstractException, SQLException {
        userService.lockUserByLogin(NULL_STR);
    }

    @Test
    public void testRemoveByEmail() throws AbstractException, SQLException {
        for (final User user : users) {
            assertTrue(userService.existById(user.getId()));
            assertNotEquals(0, projectService.findAll(user.getId()).size());
            assertNotEquals(0, taskService.findAll(user.getId()).size());
            userService.removeByEmail(user.getEmail());
            assertFalse(userService.existById(user.getId()));
            assertEquals(0, projectService.findAll(user.getId()).size());
            assertEquals(0, taskService.findAll(user.getId()).size());
        }
    }

    @Test(expected = UserNotFoundException.class)
    public void testRemoveOne() throws AbstractException, SQLException {
        userService.removeOne(null);
    }

    @Test
    public void testRemoveByLogin() throws AbstractException, SQLException {
        for (final User user : users) {
            assertTrue(userService.existById(user.getId()));
            assertNotEquals(0, projectService.findAll(user.getId()).size());
            assertNotEquals(0, taskService.findAll(user.getId()).size());
            userService.removeByLogin(user.getLogin());
            assertFalse(userService.existById(user.getId()));
            assertEquals(0, projectService.findAll(user.getId()).size());
            assertEquals(0, taskService.findAll(user.getId()).size());
        }
    }

    @Test
    public void testSetPassword() throws AbstractException, SQLException {
        for (final User user : users) {
            final String oldHash = user.getPasswordHash();
            userService.setPassword(user.getId(), "NEW_PASS" + user.getLogin());
            assertNotEquals(oldHash, userService.findOneById(user.getId()).getPasswordHash());
        }
    }

    @Test(expected = PasswordEmptyException.class)
    public void testSetPassword_PasswordEmptyException_1() throws AbstractException, SQLException {
        userService.setPassword(users.get(0).getId(), "");
    }


    @Test(expected = PasswordEmptyException.class)
    public void testSetPassword_PasswordEmptyException_2() throws AbstractException, SQLException {
        userService.setPassword(users.get(0).getId(), NULL_STR);
    }

    @Test
    public void testUnlockUserByLogin() throws AbstractException, SQLException {
        for (final User user : users) {
            user.setLocked(true);
            assertTrue(user.isLocked());
            userService.unlockUserByLogin(user.getLogin());
            assertFalse(userService.findOneById(user.getId()).isLocked());
        }
    }

    @Test(expected = LoginEmptyException.class)
    public void testUnlockUserByLogin_LoginEmptyException_1() throws AbstractException, SQLException {
        userService.unlockUserByLogin("");
    }

    @Test(expected = LoginEmptyException.class)
    public void testUnlockUserByLogin_LoginEmptyException_2() throws AbstractException, SQLException {
        userService.unlockUserByLogin(NULL_STR);
    }

    @Test(expected = LoginEmptyException.class)
    public void testLoginEmptyUnlockUserByLogin() throws AbstractException, SQLException {
        userService.unlockUserByLogin("");
    }

    @Test
    public void testUserUpdateById() throws AbstractException, SQLException {
        for (final User user : users) {
            final String firstName = user.getFirstName() + "TEST";
            final String lastName = user.getLastName() + "TEST";
            final String middleName = user.getMiddleName() + "TEST";
            assertNotEquals(user.getFirstName(), firstName);
            assertNotEquals(user.getLastName(), lastName);
            assertNotEquals(user.getMiddleName(), middleName);
            userService.updateUserById(user.getId(), firstName, lastName, middleName);
            @NotNull final User actualUser = userService.findOneById(user.getId());
            assertEquals(firstName, actualUser.getFirstName());
            assertEquals(lastName, actualUser.getLastName());
            assertEquals(middleName, actualUser.getMiddleName());
        }
    }

    @Test(expected = IdEmptyException.class)
    public void testUserUpdate_IdEmptyException_1() throws AbstractException, SQLException {
        userService.updateUserById("", "firstName", "lastName", "middleName");
    }

    @Test(expected = IdEmptyException.class)
    public void testUserUpdate_IdEmptyException_2() throws AbstractException, SQLException {
        userService.updateUserById(NULL_STR, "firstName", "lastName", "middleName");
    }

}
