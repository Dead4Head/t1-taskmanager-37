package ru.t1.amsmirnov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.repository.IRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.api.service.IService;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.IdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.IndexIncorrectException;
import ru.t1.amsmirnov.taskmanager.model.AbstractModel;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected abstract R getRepository(@NotNull final Connection connection);

    @NotNull
    @Override
    public M add(@Nullable final M model) throws AbstractException, SQLException {
        if (model == null) throw new ModelNotFoundException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final R repository = getRepository(connection);
            return repository.add(model);
        }
    }

    @NotNull
    @Override
    public Collection<M> addAll(@Nullable final Collection<M> models) throws AbstractException, SQLException {
        if (models == null) throw new ModelNotFoundException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final R repository = getRepository(connection);
            return repository.addAll(models);
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public Collection<M> set(@Nullable final Collection<M> models) throws AbstractException, SQLException {
        if (models == null) throw new ModelNotFoundException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final R repository = getRepository(connection);
            return repository.set(models);
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public List<M> findAll() throws AbstractException, SQLException {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final R repository = getRepository(connection);
            @Nullable final List<M> records = repository.findAll();
            if (records == null) throw new ModelNotFoundException();
            return records;
        }
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) throws AbstractException, SQLException {
        if (comparator == null) return findAll();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final R repository = getRepository(connection);
            @NotNull final List<M> records = repository.findAll(comparator);
            return records;
        }
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final R repository = getRepository(connection);
            repository.removeAll();
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAll(@Nullable final Collection<M> collection) throws SQLException {
        if (collection == null) {
            removeAll();
            return;
        }
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final R repository = getRepository(connection);
            repository.removeAll(collection);
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public M findOneById(@Nullable final String id) throws AbstractException, SQLException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final R repository = getRepository(connection);
            @Nullable M model = repository.findOneById(id);
            if (model == null) throw new ModelNotFoundException();
            return model;
        }
    }

    @NotNull
    @Override
    public M findOneByIndex(@Nullable final Integer index) throws AbstractException, SQLException {
        if (index == null) throw new IndexIncorrectException();
        if (index < 0 || index > getSize()) throw new IndexIncorrectException();

        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final R repository = getRepository(connection);
            @Nullable M model = repository.findOneByIndex(index);
            if (model == null) throw new ModelNotFoundException();
            return model;
        }
    }

    @NotNull
    @Override
    public M update(@Nullable final M model) throws AbstractException, SQLException {
        if (model == null) throw new ModelNotFoundException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final R repository = getRepository(connection);
            return repository.update(model);
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public M removeOne(@Nullable final M model) throws AbstractException, SQLException {
        if (model == null) throw new ModelNotFoundException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final R repository = getRepository(connection);
            @NotNull M deletedModel = repository.removeOne(model);
            return deletedModel;
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public M removeOneById(@Nullable final String id) throws AbstractException, SQLException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final R repository = getRepository(connection);
            @Nullable M deletedModel = repository.removeOneById(id);
            if (deletedModel == null) throw new ModelNotFoundException();
            return deletedModel;
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public M removeOneByIndex(@Nullable final Integer index) throws AbstractException, SQLException {
        if (index == null) throw new IndexIncorrectException();
        if (index < 0 || index > getSize()) throw new IndexIncorrectException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final R repository = getRepository(connection);
            @Nullable M deletedModel = repository.removeOneByIndex(index);
            if (deletedModel == null) throw new ModelNotFoundException();
            return deletedModel;
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public int getSize() throws SQLException {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final R repository = getRepository(connection);
            return repository.getSize();
        }
    }

    @Override
    public boolean existById(@Nullable final String id) throws AbstractException, SQLException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = getConnection()){
            @NotNull final R repository = getRepository(connection);
            return repository.existById(id);
        }
    }

    @NotNull
    public Connection getConnection() {
        return connectionService.getConnection();
    }

}
