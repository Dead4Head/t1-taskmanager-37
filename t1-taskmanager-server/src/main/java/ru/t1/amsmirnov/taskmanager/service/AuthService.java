package ru.t1.amsmirnov.taskmanager.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.service.IAuthService;
import ru.t1.amsmirnov.taskmanager.api.service.IPropertyService;
import ru.t1.amsmirnov.taskmanager.api.service.ISessionService;
import ru.t1.amsmirnov.taskmanager.api.service.IUserService;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.UserNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.LoginEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.PasswordEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.user.AccessDeniedException;
import ru.t1.amsmirnov.taskmanager.exception.util.CryptException;
import ru.t1.amsmirnov.taskmanager.model.Session;
import ru.t1.amsmirnov.taskmanager.model.User;
import ru.t1.amsmirnov.taskmanager.util.CryptUtil;
import ru.t1.amsmirnov.taskmanager.util.HashUtil;

import java.sql.SQLException;
import java.util.Date;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ISessionService sessionService;

    @Nullable
    private String userId;

    public AuthService(
            @NotNull final IUserService userService,
            @NotNull final IPropertyService propertyService,
            @NotNull final ISessionService sessionService
    ) {
        this.userService = userService;
        this.propertyService = propertyService;
        this.sessionService = sessionService;
    }

    private String getToken(@NotNull final Session session) throws JsonProcessingException, CryptException {
        @NotNull ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    private String getToken(@NotNull final User user) throws JsonProcessingException, AbstractException, CryptException, SQLException {
        return getToken(createSession(user));
    }

    private Session createSession(@NotNull final User user) throws AbstractException, SQLException {
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setRole(user.getRole());
        sessionService.add(session);
        return session;
    }


    @NotNull
    @Override
    public User registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws AbstractException, SQLException {
        return userService.create(login, password, email);
    }

    @NotNull
    @Override
    public String login(
            @Nullable final String login,
            @Nullable final String password
    ) throws AbstractException, SQLException {
        if (login == null || login.isEmpty())
            throw new LoginEmptyException();
        if (password == null || password.isEmpty())
            throw new PasswordEmptyException();
        @NotNull final User user;
        try {
            user = userService.findOneByLogin(login);
        } catch (final UserNotFoundException e) {
            throw new AccessDeniedException();
        }
        if (user.getPasswordHash() == null)
            throw new AccessDeniedException();
        if (user.isLocked())
            throw new AccessDeniedException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (!user.getPasswordHash().equals(hash))
            throw new AccessDeniedException();
        userId = user.getId();
        @NotNull final String token;
        try {
            token = getToken(user);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException(e);
        }
        return token;
    }

    @Override
    public void logout(@Nullable final Session session) throws AbstractException, SQLException {
        if (session == null) return;
        sessionService.removeOne(session);
    }

    @NotNull
    @Override
    public Session validateToken(@Nullable String token) throws AbstractException {
        if (token == null) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull String json;
        @NotNull final Session session;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            session = objectMapper.readValue(json, Session.class);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getCreated();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        final int timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();

        return session;
    }

}
