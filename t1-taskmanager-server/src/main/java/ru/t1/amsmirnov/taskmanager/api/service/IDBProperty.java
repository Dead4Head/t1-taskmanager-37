package ru.t1.amsmirnov.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDBProperty {

    @NotNull
    String getDatabaseUser();

    @NotNull
    String getDatabasePassword();

    @NotNull
    String getDatabaseUrl();

}
