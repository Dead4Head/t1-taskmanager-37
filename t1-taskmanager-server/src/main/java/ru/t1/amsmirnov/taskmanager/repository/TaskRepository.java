package ru.t1.amsmirnov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.repository.ITaskRepository;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.enumerated.database.DBCost;
import ru.t1.amsmirnov.taskmanager.model.Task;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    private final String tableName = DBCost.TABLE_TASK.getDisplayName();

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected Task fetch(@NotNull final ResultSet row) throws SQLException {
        @NotNull final Task task = new Task();
        task.setId(row.getString("id"));
        task.setUserId(row.getString(DBCost.COLUMN_USER_ID.getDisplayName()));
        task.setCreated(row.getTimestamp(DBCost.COLUMN_CREATED.getDisplayName()));
        task.setStatus(Status.toStatus(row.getString(DBCost.COLUMN_STATUS.getDisplayName())));
        task.setName(row.getString(DBCost.COLUMN_NAME.getDisplayName()));
        task.setDescription(row.getString(DBCost.COLUMN_DESCRIPTION.getDisplayName()));
        task.setProjectId(row.getString(DBCost.COLUMN_PROJECT_ID.getDisplayName()));
        return task;

    }

    @NotNull
    @Override
    public Task add(@NotNull final Task task) throws SQLException {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id, %s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?, ?)"
                , getTableName(),
                DBCost.COLUMN_CREATED.getDisplayName(),
                DBCost.COLUMN_NAME.getDisplayName(),
                DBCost.COLUMN_DESCRIPTION.getDisplayName(),
                DBCost.COLUMN_STATUS.getDisplayName(),
                DBCost.COLUMN_USER_ID.getDisplayName(),
                DBCost.COLUMN_PROJECT_ID.getDisplayName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getId());
            statement.setTimestamp(2, new Timestamp(task.getCreated().getTime()));
            statement.setString(3, task.getName());
            statement.setString(4, task.getDescription());
            statement.setString(5, task.getStatus().toString());
            statement.setString(6, task.getUserId());
            statement.setString(7, task.getProjectId());
            statement.executeUpdate();
            connection.commit();
        }
        return task;
    }

    @NotNull
    @Override
    public Task add(
            @NotNull final String userId,
            @NotNull final Task task
    ) throws SQLException {
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public Collection<Task> addAll(@NotNull final Collection<Task> tasks) throws SQLException {
        for (@NotNull final Task task : tasks)
            add(task);
        return tasks;
    }

    @NotNull
    @Override
    public Task update(@NotNull final Task task) throws SQLException {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(),
                DBCost.COLUMN_NAME.getDisplayName(),
                DBCost.COLUMN_DESCRIPTION.getDisplayName(),
                DBCost.COLUMN_STATUS.getDisplayName(),
                DBCost.COLUMN_USER_ID.getDisplayName(),
                DBCost.COLUMN_PROJECT_ID.getDisplayName(),
                DBCost.COLUMN_ID.getDisplayName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getName());
            statement.setString(2, task.getDescription());
            statement.setString(3, task.getStatus().toString());
            statement.setString(4, task.getUserId());
            statement.setString(5, task.getProjectId());
            statement.setString(6, task.getId());

            statement.executeUpdate();
            connection.commit();
        }
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) throws SQLException {
        @NotNull final List<Task> resultList = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? AND %s = ?",
                getTableName(),
                DBCost.COLUMN_PROJECT_ID.getDisplayName(),
                DBCost.COLUMN_USER_ID.getDisplayName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, projectId);
            statement.setString(2, userId);
            try (@NotNull final ResultSet result = statement.executeQuery()) {
                while (result.next())
                    resultList.add(fetch(result));
            }
        }
        return resultList;
    }

    @NotNull
    @Override
    public String getTableName() {
        return tableName;
    }

}
