package ru.t1.amsmirnov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.repository.IRepository;
import ru.t1.amsmirnov.taskmanager.comparator.NameComparator;
import ru.t1.amsmirnov.taskmanager.comparator.StatusComparator;
import ru.t1.amsmirnov.taskmanager.enumerated.ProjectSort;
import ru.t1.amsmirnov.taskmanager.enumerated.database.DBCost;
import ru.t1.amsmirnov.taskmanager.model.AbstractModel;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final Connection connection;

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    protected abstract String getTableName();

    protected abstract M fetch(@NotNull final ResultSet row) throws SQLException;

    @Nullable
    protected String getComparator(@Nullable final Comparator comparator) {
        if (comparator instanceof NameComparator) return DBCost.COLUMN_NAME.getDisplayName();
        if (comparator instanceof StatusComparator) return DBCost.COLUMN_STATUS.getDisplayName();
        return DBCost.COLUMN_CREATED.getDisplayName();
    }

    @NotNull
    @Override
    public abstract M add(@NotNull final M model) throws SQLException;

    @NotNull
    @Override
    public abstract M update(
            @NotNull final M model
    ) throws SQLException;

    @NotNull
    @Override
    public abstract Collection<M> addAll(@NotNull final Collection<M> models) throws SQLException;

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) throws SQLException {
        removeAll();
        return addAll(models);
    }

    @NotNull
    @Override
    public List<M> findAll() throws SQLException {
        @NotNull final List<M> resultList = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet result = statement.executeQuery(sql);
            while (result.next())
                resultList.add(fetch(result));
        }
        return resultList;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) throws SQLException {
        @NotNull final List<M> resultList = new ArrayList<>();
        if (getComparator(comparator) == null) return findAll();
        @NotNull String sql = String.format("SELECT * FROM %s ORDER BY %s", getTableName(), getComparator(comparator));
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet result = statement.executeQuery(sql);
            while (result.next())
                resultList.add(fetch(result));
        }
        return resultList;
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) throws SQLException {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? LIMIT 1",
                getTableName(),
                DBCost.COLUMN_ID.getDisplayName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            @NotNull final ResultSet result = statement.executeQuery();
            if (!result.next()) return null;
            return fetch(result);
        }
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final Integer index) throws SQLException {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s ORDER BY %s ASC LIMIT 1 OFFSET ?",
                getTableName(),
                DBCost.COLUMN_CREATED.getDisplayName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, index);
            @NotNull final ResultSet result = statement.executeQuery();
            if (!result.next()) return null;
            return fetch(result);
        }
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final String sql = String.format("DELETE FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
            connection.commit();
        }
    }

    @Override
    public void removeAll(@NotNull final Collection<M> collection) throws SQLException {
        for (M model : collection)
            removeOne(model);
    }


    @NotNull
    @Override
    public M removeOne(@NotNull final M model) throws SQLException {
        @NotNull final String sql = String.format(
                "DELETE FROM %s WHERE %s = ?", getTableName(), DBCost.COLUMN_ID.getDisplayName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.executeUpdate();
            connection.commit();
        }
        return model;
    }

    @Nullable
    @Override
    public M removeOneById(@NotNull final String id) throws SQLException {
        @Nullable final M model = findOneById(id);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Nullable
    @Override
    public M removeOneByIndex(@NotNull final Integer index) throws SQLException {
        @Nullable final M model = findOneByIndex(index);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Override
    public int getSize() throws SQLException {
        @NotNull final String sql = String.format("SELECT COUNT(*) as count FROM %s ", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet result = statement.executeQuery(sql);
            result.next();
            return result.getInt("count");
        }
    }

    @Override
    public boolean existById(@NotNull final String id) throws SQLException {
        @Nullable final M model = findOneById(id);
        return !(model == null);
    }

}
