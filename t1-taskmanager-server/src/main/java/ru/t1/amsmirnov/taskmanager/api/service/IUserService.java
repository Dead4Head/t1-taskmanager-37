package ru.t1.amsmirnov.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.User;

import java.sql.SQLException;

public interface IUserService extends IService<User> {

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    ) throws AbstractException, SQLException;

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email,
            @Nullable Role role
    ) throws AbstractException, SQLException;

    @NotNull
    User removeByLogin(@Nullable String login) throws AbstractException, SQLException;

    @NotNull
    User removeByEmail(@Nullable String email) throws AbstractException, SQLException;

    @NotNull
    User setPassword(
            @Nullable String id,
            @Nullable String password
    ) throws AbstractException, SQLException;

    @NotNull
    User updateUserById(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    ) throws AbstractException, SQLException;

    void lockUserByLogin(@Nullable String login) throws AbstractException, SQLException;

    void unlockUserByLogin(@Nullable String login) throws AbstractException, SQLException;

    @NotNull
    User findOneByLogin(@Nullable String login) throws AbstractException, SQLException;

    @NotNull
    User findOneByEmail(@Nullable String email) throws AbstractException, SQLException;

    boolean isLoginExist(@Nullable String login) throws AbstractException, SQLException;

    boolean isEmailExist(@Nullable String email) throws AbstractException, SQLException;

}
