package ru.t1.amsmirnov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.repository.IProjectRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.enumerated.database.DBCost;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.model.Project;

import java.sql.*;
import java.util.Collection;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    private final String tableName = DBCost.TABLE_PROJECT.getDisplayName();

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return tableName;
    }

    @NotNull
    @Override
    protected Project fetch(@NotNull final ResultSet row) throws SQLException {
        @NotNull final Project project = new Project();
        project.setId(row.getString("id"));
        project.setUserId(row.getString(DBCost.COLUMN_USER_ID.getDisplayName()));
        project.setCreated(row.getTimestamp(DBCost.COLUMN_CREATED.getDisplayName()));
        project.setStatus(Status.toStatus(row.getString(DBCost.COLUMN_STATUS.getDisplayName())));
        project.setName(row.getString(DBCost.COLUMN_NAME.getDisplayName()));
        project.setDescription(row.getString(DBCost.COLUMN_DESCRIPTION.getDisplayName()));
        return project;
    }

    @NotNull
    @Override
    public Project add(@NotNull final Project project) throws SQLException {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?)",
                getTableName(),
                DBCost.COLUMN_CREATED.getDisplayName(),
                DBCost.COLUMN_NAME.getDisplayName(),
                DBCost.COLUMN_DESCRIPTION.getDisplayName(),
                DBCost.COLUMN_STATUS.getDisplayName(),
                DBCost.COLUMN_USER_ID.getDisplayName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getId());
            statement.setTimestamp(2, new Timestamp(project.getCreated().getTime()));
            statement.setString(3, project.getName());
            statement.setString(4, project.getDescription());
            statement.setString(5, project.getStatus().toString());
            statement.setString(6, project.getUserId());
            statement.executeUpdate();
            connection.commit();
        }
        return project;
    }

    @NotNull
    @Override
    public Project add(@NotNull final String userId, @NotNull final Project project) throws SQLException {
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public Collection<Project> addAll(@NotNull final Collection<Project> projects) throws SQLException {
        for (@NotNull final Project project : projects)
            add(project);
        return projects;
    }

    @NotNull
    @Override
    public Project update(@NotNull final Project project) throws SQLException {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(),
                DBCost.COLUMN_NAME.getDisplayName(),
                DBCost.COLUMN_DESCRIPTION.getDisplayName(),
                DBCost.COLUMN_STATUS.getDisplayName(),
                DBCost.COLUMN_USER_ID.getDisplayName(),
                DBCost.COLUMN_ID.getDisplayName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getName());
            statement.setString(2, project.getDescription());
            statement.setString(3, project.getStatus().toString());
            statement.setString(4, project.getUserId());
            statement.setString(5, project.getId());
            statement.executeUpdate();
            connection.commit();
        }
        return project;
    }

}
