package ru.t1.amsmirnov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.repository.IUserOwnedRepository;
import ru.t1.amsmirnov.taskmanager.enumerated.database.DBCost;
import ru.t1.amsmirnov.taskmanager.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @NotNull
    private final String user_id_column = DBCost.COLUMN_USER_ID.getDisplayName();

    public AbstractUserOwnedRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public abstract M add(
            @NotNull final String userId,
            @NotNull final M model
    ) throws SQLException;

    @NotNull
    @Override
    public M update(
            @NotNull final String userId,
            @NotNull final M model
    ) throws SQLException {
        if (userId.equals(model.getUserId()))
            return update(model);
        return model;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) throws SQLException {
        @NotNull final List<M> resultList = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ?", getTableName(), user_id_column);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet result = statement.executeQuery();
            while (result.next())
                resultList.add(fetch(result));
        }
        return resultList;
    }

    @NotNull
    @Override
    public List<M> findAll(
            @NotNull final String userId,
            @Nullable final Comparator<M> comparator
    ) throws SQLException {
        @NotNull final List<M> resultList = new ArrayList<>();
        if (getComparator(comparator) == null) return findAll(userId);
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? ORDER BY %s", getTableName(), user_id_column, getComparator(comparator));
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet result = statement.executeQuery();
            while (result.next())
                resultList.add(fetch(result));
        }
        return resultList;
    }

    @Nullable
    @Override
    public M findOneById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws SQLException {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? AND %s = ? LIMIT 1",
                getTableName(), user_id_column, DBCost.COLUMN_ID.getDisplayName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, id);
            @NotNull final ResultSet result = statement.executeQuery();
            if (result.next())
                return fetch(result);
            return null;
        }
    }

    @Nullable
    @Override
    public M findOneByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) throws SQLException {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? ORDER BY %s ASC LIMIT 1 OFFSET ?",
                getTableName(),
                user_id_column,
                DBCost.COLUMN_CREATED.getDisplayName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setInt(2, index);
            @NotNull final ResultSet result = statement.executeQuery();
            if (result.next())
                return fetch(result);
            return null;
        }
    }

    @Override
    public void removeAll(@NotNull final String userId) throws SQLException {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE %s = ?", getTableName(), user_id_column);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.executeUpdate();
            connection.commit();
        }
    }

    @Nullable
    @Override
    public M removeOne(
            @NotNull final String userId,
            @NotNull final M model
    ) throws SQLException {
        return removeOneById(userId, model.getId());
    }

    @Nullable
    @Override
    public M removeOneById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws SQLException {
        final M model = findOneById(userId, id);
        if (model == null) return null;
        return removeOne(model);
    }

    @Nullable
    @Override
    public M removeOneByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) throws SQLException {
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return removeOne(model);
    }

    @Override
    public boolean existById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws SQLException {
        return findOneById(userId, id) != null;
    }

    @Override
    public int getSize(@NotNull final String userId) throws SQLException {
        @NotNull final String sql = String.format("SELECT COUNT(*) AS count FROM %s WHERE %s = ?", getTableName(), user_id_column);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet result = statement.executeQuery();
            result.next();
            return result.getInt("count");
        }
    }

}
