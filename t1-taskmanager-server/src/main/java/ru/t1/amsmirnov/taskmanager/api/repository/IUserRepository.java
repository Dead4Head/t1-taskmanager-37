package ru.t1.amsmirnov.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.User;

import java.sql.SQLException;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findOneByLogin(@NotNull String login) throws SQLException;

    @Nullable
    User findOneByEmail(@NotNull String email) throws SQLException;

    boolean isLoginExist(@NotNull String login) throws SQLException;

    boolean isEmailExist(@NotNull String email) throws SQLException;

}
