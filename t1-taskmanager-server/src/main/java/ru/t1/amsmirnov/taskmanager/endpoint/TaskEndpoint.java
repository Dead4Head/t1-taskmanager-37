package ru.t1.amsmirnov.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.endpoint.ITaskEndpoint;
import ru.t1.amsmirnov.taskmanager.api.service.IServiceLocator;
import ru.t1.amsmirnov.taskmanager.dto.request.task.*;
import ru.t1.amsmirnov.taskmanager.dto.response.task.*;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.enumerated.TaskSort;
import ru.t1.amsmirnov.taskmanager.model.Session;
import ru.t1.amsmirnov.taskmanager.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Comparator;
import java.util.List;

@WebService(endpointInterface = "ru.t1.amsmirnov.taskmanager.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIdRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            @Nullable final Status status = request.getStatus();
            @NotNull final Task task = getServiceLocator().getTaskService().changeStatusById(userId, id, status);
            return new TaskChangeStatusByIdResponse(task);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskChangeStatusByIdResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByIndexResponse changeTaskStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIndexRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final Integer index = request.getIndex();
            @Nullable final Status status = request.getStatus();
            @NotNull final Task task = getServiceLocator().getTaskService().changeStatusByIndex(userId, index, status);
            return new TaskChangeStatusByIndexResponse(task);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskChangeStatusByIndexResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskClearResponse removeAllTasks(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskClearRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            getServiceLocator().getTaskService().removeAll(userId);
            return new TaskClearResponse();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskClearResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCompleteByIdResponse completeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCompleteByIdRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            @NotNull final Task task = getServiceLocator().getTaskService().changeStatusById(userId, id, Status.COMPLETED);
            return new TaskCompleteByIdResponse(task);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskCompleteByIdResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCompleteByIndexResponse completeTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCompleteByIndexRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final Integer index = request.getIndex();
            @NotNull final Task task = getServiceLocator().getTaskService().changeStatusByIndex(userId, index, Status.COMPLETED);
            return new TaskCompleteByIndexResponse(task);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskCompleteByIndexResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCreateRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String name = request.getName();
            @Nullable final String description = request.getDescription();
            @NotNull final Task task = getServiceLocator().getTaskService().create(userId, name, description);
            return new TaskCreateResponse(task);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskCreateResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskListResponse findAllTasks(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskListRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final TaskSort sort = request.getSort();
            Comparator<Task> comparator = null;
            if (sort != null)
                comparator = sort.getComparator();
            @NotNull final List<Task> tasks = getServiceLocator().getTaskService().findAll(userId, comparator);
            return new TaskListResponse(tasks);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskListResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByIdResponse removeOneTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIdRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            @NotNull final Task task = getServiceLocator().getTaskService().removeOneById(userId, id);
            return new TaskRemoveByIdResponse(task);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskRemoveByIdResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByIndexResponse removeOneTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIndexRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final Integer index = request.getIndex();
            @NotNull final Task task = getServiceLocator().getTaskService().removeOneByIndex(userId, index);
            return new TaskRemoveByIndexResponse(task);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskRemoveByIndexResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowByIdResponse findOneTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowByIdRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            @NotNull final Task task = getServiceLocator().getTaskService().findOneById(userId, id);
            return new TaskShowByIdResponse(task);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskShowByIdResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowByIndexResponse findOneTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowByIndexRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final Integer index = request.getIndex();
            @NotNull final Task task = getServiceLocator().getTaskService().findOneByIndex(userId, index);
            return new TaskShowByIndexResponse(task);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskShowByIndexResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskStartByIdResponse startTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskStartByIdRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            @NotNull final Task task = getServiceLocator().getTaskService().changeStatusById(userId, id, Status.IN_PROGRESS);
            return new TaskStartByIdResponse(task);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskStartByIdResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskStartByIndexResponse startTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskStartByIndexRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final Integer index = request.getIndex();
            @NotNull final Task task = getServiceLocator().getTaskService().changeStatusByIndex(userId, index, Status.IN_PROGRESS);
            return new TaskStartByIndexResponse(task);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskStartByIndexResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIdRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            @Nullable final String name = request.getName();
            @Nullable final String description = request.getDescription();
            @NotNull final Task task = getServiceLocator().getTaskService().updateById(userId, id, name, description);
            return new TaskUpdateByIdResponse(task);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskUpdateByIdResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUpdateByIndexResponse updateTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIndexRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final Integer index = request.getIndex();
            @Nullable final String name = request.getName();
            @Nullable final String description = request.getDescription();
            @NotNull final Task task = getServiceLocator().getTaskService().updateByIndex(userId, index, name, description);
            return new TaskUpdateByIndexResponse(task);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskUpdateByIndexResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskBindToProjectRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String projectId = request.getProjectId();
            @Nullable final String taskId = request.getTaskId();
            getServiceLocator().getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
            return new TaskBindToProjectResponse();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskBindToProjectResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUnbindFromProjectResponse unbindTaskFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUnbindFromProjectRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String projectId = request.getProjectId();
            @Nullable final String taskId = request.getTaskId();
            getServiceLocator().getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
            return new TaskUnbindFromProjectResponse();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskUnbindFromProjectResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowByProjectIdResponse findAllTasksByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskShowByProjectIdRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String projectId = request.getProjectId();
            @NotNull final List<Task> tasks = getServiceLocator().getTaskService().findAllByProjectId(userId, projectId);
            return new TaskShowByProjectIdResponse(tasks);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskShowByProjectIdResponse(e);
        }
    }

}
