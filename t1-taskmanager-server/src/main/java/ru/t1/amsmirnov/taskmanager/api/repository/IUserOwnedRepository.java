package ru.t1.amsmirnov.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.AbstractUserOwnedModel;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @NotNull
    M add(@NotNull String userId, @NotNull M model) throws SQLException;

    @NotNull
    List<M> findAll(@NotNull String userId) throws SQLException;

    @NotNull
    List<M> findAll(@NotNull String userId, @Nullable Comparator<M> comparator) throws SQLException;

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id) throws SQLException;

    @Nullable
    M findOneByIndex(@NotNull String userId, @NotNull Integer index) throws SQLException;

    @NotNull
    M update(@NotNull String userId, @NotNull M model) throws SQLException;

    @Nullable
    M removeOne(@NotNull String userId, @NotNull M model) throws SQLException;

    @Nullable
    M removeOneById(@NotNull String userId, @NotNull String id) throws SQLException;

    @Nullable
    M removeOneByIndex(@NotNull String userId, @NotNull Integer index) throws SQLException;

    boolean existById(@NotNull String userId, @NotNull String id) throws SQLException;

    void removeAll(@NotNull String userId) throws SQLException;

    int getSize(@NotNull String userId) throws SQLException;

}
