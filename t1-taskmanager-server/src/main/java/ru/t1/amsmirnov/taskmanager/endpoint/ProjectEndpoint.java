package ru.t1.amsmirnov.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.endpoint.IProjectEndpoint;
import ru.t1.amsmirnov.taskmanager.api.service.IServiceLocator;
import ru.t1.amsmirnov.taskmanager.dto.request.project.*;
import ru.t1.amsmirnov.taskmanager.dto.response.project.*;
import ru.t1.amsmirnov.taskmanager.enumerated.ProjectSort;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.Project;
import ru.t1.amsmirnov.taskmanager.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Comparator;
import java.util.List;

@WebService(endpointInterface = "ru.t1.amsmirnov.taskmanager.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectChangeStatusByIdRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String projectId = request.getId();
            @Nullable final Status status = request.getStatus();
            @Nullable final String userId = session.getUserId();
            @NotNull final Project project = getServiceLocator().getProjectService().changeStatusById(userId, projectId, status);
            return new ProjectChangeStatusByIdResponse(project);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectChangeStatusByIdResponse(e);
        }
    }


    @NotNull
    @Override
    @WebMethod
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectChangeStatusByIndexRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final Integer index = request.getIndex();
            @Nullable final Status status = request.getStatus();
            @Nullable final String userId = session.getUserId();
            @NotNull final Project project = getServiceLocator().getProjectService().changeStatusByIndex(userId, index, status);
            return new ProjectChangeStatusByIndexResponse(project);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectChangeStatusByIndexResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectClearResponse removeAllProjects(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectClearRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            getServiceLocator().getProjectService().removeAll(userId);
            return new ProjectClearResponse();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectClearResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCompleteByIdResponse completeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCompleteByIdRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            @NotNull Project project = getServiceLocator().getProjectService().changeStatusById(userId, id, Status.COMPLETED);
            return new ProjectCompleteByIdResponse(project);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectCompleteByIdResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCompleteByIndexResponse completeProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCompleteByIndexRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final Integer index = request.getIndex();
            @NotNull Project project = getServiceLocator().getProjectService().changeStatusByIndex(userId, index, Status.COMPLETED);
            return new ProjectCompleteByIndexResponse(project);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectCompleteByIndexResponse(e);
        }
    }


    @NotNull
    @Override
    @WebMethod
    public ProjectCreateResponse createProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCreateRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String name = request.getName();
            @Nullable final String description = request.getDescription();
            @NotNull Project project = getServiceLocator().getProjectService().create(userId, name, description);
            return new ProjectCreateResponse(project);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectCreateResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectListResponse findAllProjects(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectListRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final ProjectSort sort = request.getSort();
            Comparator<Project> comparator = null;
            if (sort != null)
                comparator = sort.getComparator();
            @NotNull List<Project> projects = getServiceLocator().getProjectService().findAll(userId, comparator);
            return new ProjectListResponse(projects);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectListResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectRemoveByIdResponse removeOneProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectRemoveByIdRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            @NotNull Project project = getServiceLocator().getProjectService().removeOneById(userId, id);
            return new ProjectRemoveByIdResponse(project);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectRemoveByIdResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectRemoveByIndexResponse removeOneProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectRemoveByIndexRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final Integer index = request.getIndex();
            @NotNull Project project = getServiceLocator().getProjectService().removeOneByIndex(userId, index);
            return new ProjectRemoveByIndexResponse(project);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectRemoveByIndexResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectShowByIdResponse findOneProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectShowByIdRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            @NotNull Project project = getServiceLocator().getProjectService().findOneById(userId, id);
            return new ProjectShowByIdResponse(project);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectShowByIdResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectShowByIndexResponse findOneProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectShowByIndexRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final Integer index = request.getIndex();
            @NotNull Project project = getServiceLocator().getProjectService().findOneByIndex(userId, index);
            return new ProjectShowByIndexResponse(project);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectShowByIndexResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectStartByIdResponse startProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectStartByIdRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            @NotNull Project project = getServiceLocator().getProjectService().changeStatusById(userId, id, Status.IN_PROGRESS);
            return new ProjectStartByIdResponse(project);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectStartByIdResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectStartByIndexResponse startProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectStartByIndexRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final Integer index = request.getIndex();
            @NotNull Project project = getServiceLocator().getProjectService().changeStatusByIndex(userId, index, Status.IN_PROGRESS);
            return new ProjectStartByIndexResponse(project);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectStartByIndexResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectUpdateByIdResponse updateProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectUpdateByIdRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            @Nullable final String name = request.getName();
            @Nullable final String description = request.getDescription();
            @NotNull Project project = getServiceLocator().getProjectService().updateById(userId, id, name, description);
            return new ProjectUpdateByIdResponse(project);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectUpdateByIdResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectUpdateByIndexResponse updateProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectUpdateByIndexRequest request
    ) {
        try {
            @NotNull final Session session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final Integer index = request.getIndex();
            @Nullable final String name = request.getName();
            @Nullable final String description = request.getDescription();
            @NotNull Project project = getServiceLocator().getProjectService().updateByIndex(userId, index, name, description);
            return new ProjectUpdateByIndexResponse(project);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new ProjectUpdateByIndexResponse(e);
        }
    }

}
