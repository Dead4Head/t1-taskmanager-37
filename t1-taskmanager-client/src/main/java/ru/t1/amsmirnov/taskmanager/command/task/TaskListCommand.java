package ru.t1.amsmirnov.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.task.TaskListRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.task.TaskListResponse;
import ru.t1.amsmirnov.taskmanager.enumerated.TaskSort;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

import java.util.Arrays;

public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-list";

    @NotNull
    public static final String DESCRIPTION = "Show task list.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(ru.t1.amsmirnov.taskmanager.enumerated.TaskSort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @NotNull final TaskSort taskSort = TaskSort.toSort(sortType);
        @NotNull final TaskListRequest request = new TaskListRequest(getToken(), taskSort);
        @NotNull final TaskListResponse response = getTaskEndpoint().findAllTasks(request);
        if (!response.isSuccess() || response.getTasks() == null)
            throw new CommandException(response.getMessage());
        renderTasks(response.getTasks());
    }

}
