package ru.t1.amsmirnov.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.project.ProjectListRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.project.ProjectListResponse;
import ru.t1.amsmirnov.taskmanager.enumerated.ProjectSort;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;
import ru.t1.amsmirnov.taskmanager.model.Project;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-list";

    @NotNull
    public static final String DESCRIPTION = "Show project list.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT: ");
        System.out.println(Arrays.toString(ProjectSort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @NotNull final ProjectSort projectSort = ProjectSort.toSort(sortType);
        @NotNull final ProjectListRequest request = new ProjectListRequest(getToken(), projectSort);
        @NotNull final ProjectListResponse response = getProjectEndpoint().findAllProjects(request);
        if (!response.isSuccess() || response.getProjects() == null)
            throw new CommandException(response.getMessage());

        int index = 1;
        for (@Nullable final Project project : response.getProjects()) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getName());
            index++;
        }
    }

}
