package ru.t1.amsmirnov.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.task.TaskCreateRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.task.TaskCreateResponse;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-create";

    @NotNull
    public static final String DESCRIPTION = "Create new task.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER TASK NAME:");
        @NotNull final String taskName = TerminalUtil.SCANNER.nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        @NotNull final String taskDescription = TerminalUtil.SCANNER.nextLine();
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(getToken(), taskName, taskDescription);
        @NotNull final TaskCreateResponse response = getTaskEndpoint().createTask(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
