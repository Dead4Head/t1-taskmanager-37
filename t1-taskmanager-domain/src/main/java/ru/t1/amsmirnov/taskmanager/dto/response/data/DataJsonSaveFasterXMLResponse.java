package ru.t1.amsmirnov.taskmanager.dto.response.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public final class DataJsonSaveFasterXMLResponse extends AbstractResultResponse {

    public DataJsonSaveFasterXMLResponse() {
    }

    public DataJsonSaveFasterXMLResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
