package ru.t1.amsmirnov.taskmanager.dto.response.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public final class DataXmlSaveJaxBResponse extends AbstractResultResponse {

    public DataXmlSaveJaxBResponse() {
    }

    public DataXmlSaveJaxBResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
