package ru.t1.amsmirnov.taskmanager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.task.*;
import ru.t1.amsmirnov.taskmanager.dto.response.task.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.net.MalformedURLException;

@WebService
public interface ITaskEndpoint extends IEndpoint {

    @NotNull String NAME = "TaskEndpoint";

    @NotNull String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance() {
        return IEndpoint.newInstance(NAME, PART, ITaskEndpoint.class);
    }

    @NotNull
    @WebMethod
    TaskChangeStatusByIdResponse changeTaskStatusById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull TaskChangeStatusByIdRequest request);

    @NotNull
    @WebMethod
    TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull TaskChangeStatusByIndexRequest request);

    @NotNull
    @WebMethod
    TaskClearResponse removeAllTasks(@WebParam(name = REQUEST, partName = REQUEST) @NotNull TaskClearRequest request);

    @NotNull
    @WebMethod
    TaskCompleteByIdResponse completeTaskById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull TaskCompleteByIdRequest request);

    @NotNull
    @WebMethod
    TaskCompleteByIndexResponse completeTaskByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull TaskCompleteByIndexRequest request);

    @NotNull
    @WebMethod
    TaskCreateResponse createTask(@WebParam(name = REQUEST, partName = REQUEST) @NotNull TaskCreateRequest request);

    @NotNull
    @WebMethod
    TaskListResponse findAllTasks(@WebParam(name = REQUEST, partName = REQUEST) @NotNull TaskListRequest request);

    @NotNull
    @WebMethod
    TaskRemoveByIdResponse removeOneTaskById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull TaskRemoveByIdRequest request);

    @NotNull
    @WebMethod
    TaskRemoveByIndexResponse removeOneTaskByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull TaskRemoveByIndexRequest request);

    @NotNull
    @WebMethod
    TaskShowByIdResponse findOneTaskById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull TaskShowByIdRequest request);

    @NotNull
    @WebMethod
    TaskShowByIndexResponse findOneTaskByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull TaskShowByIndexRequest request);

    @NotNull
    @WebMethod
    TaskShowByProjectIdResponse findAllTasksByProjectId(@WebParam(name = REQUEST, partName = REQUEST) @NotNull TaskShowByProjectIdRequest request);

    @NotNull
    @WebMethod
    TaskStartByIdResponse startTaskById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull TaskStartByIdRequest request);

    @NotNull
    @WebMethod
    TaskStartByIndexResponse startTaskByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull TaskStartByIndexRequest request);

    @NotNull
    @WebMethod
    TaskUpdateByIdResponse updateTaskById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull TaskUpdateByIdRequest request);

    @NotNull
    @WebMethod
    TaskUpdateByIndexResponse updateTaskByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull TaskUpdateByIndexRequest request);

    @NotNull
    @WebMethod
    TaskBindToProjectResponse bindTaskToProject(@WebParam(name = REQUEST, partName = REQUEST) @NotNull TaskBindToProjectRequest request);

    @NotNull
    @WebMethod
    TaskUnbindFromProjectResponse unbindTaskFromProject(@WebParam(name = REQUEST, partName = REQUEST) @NotNull TaskUnbindFromProjectRequest request);

}
