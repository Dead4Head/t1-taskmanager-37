package ru.t1.amsmirnov.taskmanager.dto.response.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.User;

public final class UserUpdateResponse extends AbstractUserResponse {

    public UserUpdateResponse() {
    }

    public UserUpdateResponse(@Nullable final User user) {
        super(user);
    }

    public UserUpdateResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}